from bs4 import BeautifulSoup
from bs4 import NavigableString
from time import sleep, time
from random import randint
from warnings import warn
import requests
import csv
import json
import pandas as pd
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import table, column, select, update, insert

engine_internal = sqlalchemy.create_engine("mysql+mysqlconnector://%s:%s@%s/%s"
                                           % ('root', 'admin', 'localhost:3306',
                                              'jobs_db'),
                                           pool_size=3, pool_recycle=3600)

connection = engine_internal.connect()


source = requests.get('https://www.irishjobs.ie/ShowResults.aspx?Keywords=&autosuggestEndpoint=%2fautosuggest&Location=0&Category=3&Recruiter=Company%2cAgency&btnSubmit=+&PerPage=100')
soup = BeautifulSoup(source.text, 'lxml')

# lists for data
pages = []
data = [] # list of job
urls = []
# for monitoring requests
start_time = time() # to calculate elapsed time between requests.
request_count = 1  # Counter is equal to one because initial request was made when crawler executed.


# find nav with page links and use them to loop through all pages.
pageurls = soup.find('ul', id='pagination')
# pick previous last one
last_page = pageurls.find_all('li')[-2].text
last_page = int(last_page)
print('There are a total of ', last_page, 'pages')

# pick previous last one
for i in range(last_page):
    pageurl = 'https://www.irishjobs.ie/ShowResults.aspx?Keywords=&autosuggestEndpoint=%2Fautosuggest&Location=0&Category=3&Recruiter=Company&Recruiter=Agency&btnSubmit=+&PerPage=100&Page='+str(i)
    pages.append(pageurl)

print('Number of pages to be scraped: ', len(pages))
# write query and execute.
# fetch results and add them to the list from which I can compare query results against each scraped posts
query = "SELECT title, company, short_desc FROM irishjobs_posts;"
title_qry = connection.execute(query)
result_set = title_qry.fetchall()
result_list = []
# removes brakets from the items to store them in the list appropriately
for row in result_set:
    for subrow in row:
        result_list.append(subrow)
page_count = 0
post_count = 0

for page in pages:
    page_count += 1
    print('Page: ', page_count)
    source2 = requests.get(page, timeout=100.0)
    soup2 = BeautifulSoup(source2.text, 'lxml')
    # Pause
    sleep(randint(1, 3))
    # Monitor
    request_count += 1
    elapsed_time = time() - start_time
    print('Request nr: {}; Frequency: {} requests per second; Status: {}'.format(request_count, request_count/elapsed_time, source2.status_code))

    # Throw a warning for non-200 status codes
    if source2.status_code != 200:
        warn('Request: {}; Status code: {}'.format(request_count, source2.status_code))


    # all of the job posts divs
    job_posts = soup2.find_all('div', class_='job-result')

    for post in job_posts:
        job = {}
        job['title'] = post.find('h2', itemprop='title').text
        try:
            job['company'] = post.find('h3', itemprop='name').a.text
        except:
            print('Company was not found for this position: ', job['title'])
            job['company'] = "N/A"
        job['updated'] = post.find('li', class_='updated-time').text
        job['location'] = post.find('li', class_='location').text
        job['salary'] = post.find('li', class_='salary').text
        job['short_desc'] = post.find('p', itemprop='description').text
        job['url_more'] = post.find(itemprop='title').a.get('href')
        job['url_more'] = job['url_more'].replace(" ", "")
        #######
        # remove single quote from data text so it doesnt interfere with mysql query
        job['title'] = job['title'].replace("'", "").replace('\n', '')
        job['company'] = job['company'].replace("'", "").replace('\n', '')
        job['location'] = job['location'].replace("'", "").replace('\n', '')
        job['salary'] = job['salary'].replace("'", "")
        job['short_desc'] = job['short_desc'].replace("'", "").replace('\n', '')

        if job['title'] in result_list and job['company'] in result_list or job['short_desc'] in result_list:
            print('\n -- WONT SAVE THAT RECORD, IT ALREADY EXISTS  - ',job['title'])
            continue

        else:
            post_count += 1
            print('\n', post_count,'. There is no such record - ', job['title'], '-> Will be saved to the database.')

        urls.append(job['url_more'])
        data.append(job)

print('\n', len(urls), 'records to be scraped... \n')
print(data)

# create dataframe to add other information onto the same dataset
jobs_df = pd.DataFrame(data)
# create lists to populate with data from details page
full_descriptions = []
emp_types = []
# comp_types = []

# now visiting each url_more
for url in urls:
    try:
        source3 = requests.get('https://www.irishjobs.ie/'+url)
        soup3 = BeautifulSoup(source3.text, 'lxml')

    except:
        print('Cant access requested url, on to the next one')
        continue # on to the next url in the loop.

    # Pause
    sleep(randint(1, 3))
    # Monitor
    request_count += 1 # first request was made at the crawlers initiation
    elapsed_time = time() - start_time # time passed between now and start of crawling.
    print('Request nr: {}; Frequency: {} requests per second; Status: {}'.format(request_count, request_count/elapsed_time, source3.status_code))

    full_desc = soup3.find('div', class_='job-details').text
    # remove single quote from data text so it doesnt interfere with mysql query
    full_desc = full_desc.replace("'", "").replace('\n', '')
    emp_type = soup3.find('li', class_='employment-type').text

    # comp_type = soup3.find('a', class_='arrow-link-blue').text

    # if "Employer" in comp_type:
    #     comp_type = "employer"
    #
    # elif "Agency" in comp_type:
    #     comp_type = "agency"
    #

    full_descriptions.append(full_desc)
    emp_types.append(emp_type)
    # comp_types.append(comp_type)

print('Series now...')

jobs_df['full_desc'] = pd.Series(full_descriptions)
jobs_df['emp_type'] = pd.Series(emp_types)
# jobs_df['comp_type'] = pd.Series(comp_types)

data = jobs_df.to_dict('records')

with open('data/irishjobs_sample.json', 'w', encoding='utf-8') as writeJSON:
    json.dump(data, writeJSON, ensure_ascii=False)
print('\nThe DF has been successfully translated into json, and there are: ', len(data), 'records.')
print(request_count, ' requests has been made.')

print('============================================== \n \n')
print('Database now... \n \n')


query_string = ""
posts_counter = 0
last_index = len(data)

for value in data:
    posts_counter += 1
    # turn data values into str types for the query.
    title = str(value['title'])
    company = str(value['company'])
    updated = str(value['updated'])
    location = str(value['location'])
    salary = str(value['salary'])
    short_desc = str(value['short_desc'])
    full_desc = str(value['full_desc'])
    url = str(value['url_more'])
    emp_type = str(value['emp_type'])
    # comp_type = str(value['comp_type'])
    # if item is not last on the list, put comma to separate each value in the query, else finish query
    # with semi-colon.
    if posts_counter != last_index:
        end_value = ","
    else:
        end_value = ";"

    query_string += "('" + title + "', '" + company + "', '" + updated + "', '" + location + "', '" + salary + "', '" + short_desc + "', '" + full_desc + "', '" + url + "', '" + emp_type + "' )" + end_value


sql_query = "insert into irishjobs_posts (" \
                        "title, company, updated, location, salary, short_desc, full_desc, url, emp_type" \
                        ") values " + query_string

connection.execute(sql_query)
connection.close()


print('=================================')
print('IRISHJOBS.IE scraper is done.')
print('=================================')

# because the url is skipped, but we have short details it probably gives error because the full values havent been given.
# try make it so
