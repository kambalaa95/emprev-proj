from django.views.generic import TemplateView
from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    path('', analysis_home),
    path('classified_jobs/', DefItListView.as_view(), name='def_it_list'),
    path('classified_jobs/prob_it/', ProbItListView.as_view(), name='prob_it_list'),
    path('job_areas/', job_areas),

    path('pyldavis/', TemplateView.as_view(template_name="analysis_results/40topics_vis.html"), name='40_topics_vis'),

]
