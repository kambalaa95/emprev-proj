# This file creates corpus of tokens from IT jobs as well as the dictionary object.
# Files have been split to avoid waiting for lemmatization (the longest process) over and over.
import gensim
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from gensim.utils import simple_preprocess # CHANGED DICTIONARY
import json
import pandas as pd
import spacy
import re

# Load main corpus - IT job descriptions
jobs_data = open('data/all_it_jobs.json', encoding="utf8")
jobs_data = json.load(jobs_data)
jobs_data = pd.DataFrame(jobs_data)
descriptions = jobs_data['full_desc']
print("Number of documents:",len(descriptions))

# Tokenize the documents in the corpus.
tokenizer = RegexpTokenizer(r'\w+')
tok_docs = [[w.lower() for w in tokenizer.tokenize(text)]
             for text in descriptions]
print('Tokenized it job descriptions (sample):\n',tok_docs[0], '\nLEN: ',len(tok_docs[0]))

# Remove stopwords
stop_words = set(stopwords.words('english'))
tok_docs = [[w for w in tok_doc if not w in stop_words] for tok_doc in tok_docs]
print('Stop words removed:\n', tok_docs[0], '\nLEN: ',len(tok_docs[0]))

# Lemmatize tokens
nlp = spacy.load('en', disable=['parser', 'ner'])
def lemmatize(descs, tags=['NOUN', 'ADJ']): # filter noun and adjective
    output = []
    for sent in descs:
        document = nlp(" ".join(sent))
        output.append([token.lemma_ for token in document if token.pos_ in tags])
    return output
tok_docs = lemmatize(tok_docs)
print('Lemmatized description:\n', tok_docs[0], '\n LEN: ',len(tok_docs[0]))

# Save the tokenized corpus
with open('data/it_tok_docs.json', 'w') as outfile:
    json.dump(tok_docs, outfile)

# Create Dictionary
# --- FROM CORPUS ---
dictionary = gensim.corpora.Dictionary(tok_docs)
dictionary.save('tmp/it_dictionary.dict') # save
print(dictionary)
# --- FROM IT_TERMS ---
# it_terms = open('data/it_terms.json')
# it_terms = json.load(it_terms)
# dictionary = gensim.corpora.Dictionary([simple_preprocess(term) for term in it_terms])
# dictionary.save('tmp/it_dictionary.dict') # save
# print(dictionary)

# print the message
print('\nThe dictionary and the tokenized corpus have been saved successfully!')
