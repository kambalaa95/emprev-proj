from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.generic import ListView, DetailView, CreateView
from .models import AllItJobs
from django.db.models import Q


class JobListView(ListView):
    model = AllItJobs
    template_name = 'job-posts/job-search.html'
    context_object_name = 'jobs_list'
    paginate_by = 5
    jobs_count = 5

    def get_queryset(self):

        if 'title' in self.request.GET:
            objects = AllItJobs.objects.filter(
                Q(title__icontains=self.request.GET['title'])
            )
        else:
            objects = AllItJobs.objects.all()

        return objects

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['count'] = self.get_queryset().count()
        return data


class JobDetailsView(DetailView):
    model = AllItJobs
    template_name = 'job-posts/job-details.html'
    context_object_name = 'job_post'
    pk_url_kwarg = 'job_id'

    def get_object(self, query_set=None):
        object = super(JobDetailsView, self).get_object(queryset=query_set)
        # Error if object not found.
        if object is None:
            raise Http404("No such job is registered in our database.")
        return object

    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            # redirect here
            raise Http404("No such job is registered in our database.")
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

# class SearchView(ListView):
#     model = AllItJobs
#     template_name = 'job-posts/job-search.html'
#     context_object_name = 'jobs_list'
#     paginate_by = 5
#
#     def get_queryset(self):
#         return self.model.objects.filter(title__icontains=self.request.GET['title'],
#                                          location__icontains=self.request.GET['location'])
#     def get_context_data(self, **kwargs):
#         data = super().get_context_data(**kwargs)
#         data['count'] = self.get_queryset().count()
#         return data