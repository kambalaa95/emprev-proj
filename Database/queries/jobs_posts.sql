SET SQL_SAFE_UPDATES = 0;  # to enable delete


SELECT * FROM jobs_posts;

SELECT COUNT(*) from jobs_posts; # 297

# Check jobs_posts for duplicates and remove them
SELECT short_desc, COUNT(*) c FROM jobs_posts GROUP BY short_desc HAVING c > 1;

# add ID for further step
ALTER TABLE `jobs_posts` ADD `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY FIRST;
# delete duplicates
DELETE t1 FROM jobs_posts t1
        INNER JOIN
    jobs_posts t2 
WHERE
    t1.id < t2.id AND t1.short_desc = t2.short_desc;

SELECT COUNT(*) FROM jobs_posts; # 289

ALTER TABLE jobs_posts DROP COLUMN id;

