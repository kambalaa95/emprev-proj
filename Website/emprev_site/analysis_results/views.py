from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.generic import ListView, DetailView, CreateView
from .models import DefItJobs, ProbItJobs
from django.db.models import Q

# Create your views here.
def analysis_home(request):
    return render(request, 'analysis_results/analysis_home.html')

def ads_distr(request):
    return render(request, 'analysis_results/ads_distr.html')

def job_areas(request):
    return render(request, 'analysis_results/job_areas.html')

class DefItListView(ListView):
    model = DefItJobs
    template_name = 'analysis_results/def_it.html'
    context_object_name = 'def_it_list'


    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['count'] = self.get_queryset().count()
        return data

class ProbItListView(ListView):
    model = ProbItJobs
    template_name = 'analysis_results/prob_it.html'
    context_object_name = 'prob_it_list'


    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['count'] = self.get_queryset().count()
        return data