from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, DetailView, CreateView
from .models import AllItJobs

# Create your views here.
class HomeView(ListView):
    template_name = 'home.html'
    context_object_name = 'jobs'


