from django.db import models

# Create your models here.
class DefItJobs(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'definetly_it'


class ProbItJobs(models.Model):
    id = models.IntegerField(primary_key=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'probably_it'