SET SQL_SAFE_UPDATES = 0;  # to enable delete


SELECT * FROM jobs_db.test_table;
SELECT COUNT(*) from test_table; # 6672, 

INSERT IGNORE INTO test_table SELECT * FROM jobs_posts;
INSERT IGNORE INTO test_table SELECT * FROM irishjobs_posts;

SELECT COUNT(*) from test_table; # 12990

# Check for duplicates and remove them
SELECT short_desc, COUNT(*) c FROM test_table GROUP BY short_desc HAVING c > 1;
# add ID for further step
ALTER TABLE `test_table` ADD `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY FIRST;
# delete duplicates
DELETE t1 FROM test_table t1
        INNER JOIN
    test_table t2 
WHERE
    t1.id < t2.id AND t1.short_desc = t2.short_desc;


SELECT COUNT(*) FROM test_table; # 7925 unique records


ALTER TABLE test_table DROP COLUMN id;
