
from django.contrib import admin
from django.urls import path, include
from .views import *

urlpatterns = [
    # path('', views.list_jobs),
    path('', JobListView.as_view(), name='jobs-posts'),
    path('search', JobListView.as_view(), name='search'),
    # path('', HomeView.as_view(), name='home'),
    # path('<int:job_id>/', job_detail),
    path('<int:job_id>/', JobDetailsView.as_view(), name='job_post'),

]
