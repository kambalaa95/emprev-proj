from django.db import models

# Create your models here.
class Job_post(models.Model):
    title = models.CharField(max_length=300)
    company = models.CharField(max_length=100)
    updated = models.DateTimeField()
    location = models.CharField(max_length=150)
    salary = models.CharField(max_length=150)
    short_desc = models.TextField()
    full_desc = models.TextField()
    url = models.CharField(max_length=200, default="")
    emp_type = models.CharField(max_length=150)
    # category = models.CharField(max_length=100) maybe for later -- job area.


    # created_at = models.DateTimeField(default=timezone.now)
    # filled = models.BooleanField(default=False)

    def __str__(self):
        return self.title