# This program removes non utf8 \\xc2\\xa0 characters

import csv
import pandas as pd
import json
# list with unique locations
locs = pd.read_csv("data/input/filter-nonutf.csv", encoding='utf-8')
locs = locs['x'].tolist()
# remove those bloody characters locations
# print(type(u_locs['x']))
locs = [loc.replace('\\xc2\\xa0', '') for loc in locs]
print(locs)

# remove those bloody characters locations
locs = [loc.replace('\\xc2\\xa0', '') for loc in locs]
# Dump as json string
dumps_locs = json.dumps(locs)
# JSON OUTPUT
with open('data/output/filtered-nonutf.json', 'w') as f:
    f.write(dumps_locs)