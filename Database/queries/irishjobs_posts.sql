SET SQL_SAFE_UPDATES = 0;  # to enable delete


SELECT * FROM jobs_db.irishjobs_posts;

SELECT COUNT(*) from irishjobs_posts; # 4892 , 6174

# Check for duplicates and remove them
SELECT short_desc, COUNT(*) c FROM irishjobs_posts GROUP BY short_desc HAVING c > 1;
# add ID for further step
ALTER TABLE `irishjobs_posts` ADD `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY FIRST;
# delete duplicates
DELETE t1 FROM irishjobs_posts t1
        INNER JOIN
    irishjobs_posts t2 
WHERE
    t1.id < t2.id AND t1.short_desc = t2.short_desc;


SELECT COUNT(*) FROM irishjobs_posts; # 4883, 5908


ALTER TABLE irishjobs_posts DROP COLUMN id;
