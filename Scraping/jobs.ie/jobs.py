from bs4 import BeautifulSoup
from bs4 import NavigableString
import json
from time import sleep, time
from random import randint
import requests
import pandas as pd
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import table, column, select, update, insert
engine_internal = sqlalchemy.create_engine("mysql+mysqlconnector://%s:%s@%s/%s"
                                           % ('root', 'admin', 'localhost:3306',
                                              'jobs_db'),
                                           pool_size=3, pool_recycle=3600)

connection = engine_internal.connect()
# https://www.jobs.ie/it_programming_jobs.aspx?page=6
# https://www.jobs.ie/Jobs.aspx?hd_searchbutton=true&Categories=4&Regions=0&Keywords=DevOPS+engineer&job-search=true
source = requests.get('https://www.jobs.ie/it_programming_jobs.aspx?page=6')
soup = BeautifulSoup(source.text, 'lxml')

data = []
urls = []
start_time = time() # to calculate elapsed time between requests.
request_count = 1  # Counter is equal to one because initially we made the first request

# write query and execute.
# fetch results and add them to the list from which I can compare query results against each scraped posts
query = "SELECT short_desc FROM jobs_posts"
title_qry = connection.execute(query)
result_set = title_qry.fetchall()
result_list = []
# removes brakets from the items
for row in result_set:
    for subrow in row:
        result_list.append(subrow)
post_count = 0

job_posts = soup.find_all('article', class_='job-list-item')
for post in job_posts:
    job = {}
    job['title'] = post.find('span', class_='job').h2.a.text
    job['company'] = post.find('span', class_='name').h3.a.text
    job['url_more'] = post.find('span', class_='job').h2.a.get('href')
    job['location'] = post.find('span', class_='location').text
    job['short_desc'] = post.find('span', class_='snippet').a.text
    # avoid saving records which short_desc's that are already in the database
    # if title and company are the same, then don't scrape.

    # remove single quote from data text so it doesnt interfere with mysql query
    job['title'] = job['title'].replace("'", "")
    job['company'] = job['company'].replace("'", "")
    job['location'] = job['location'].replace("'", "")
    job['short_desc'] = job['short_desc'].replace("'", "")




    if job['short_desc'] in result_list:
        print('\n -- WONT SAVE THAT RECORD, IT ALREADY EXISTS  - ',job['title'])
        continue

    else:
        post_count += 1
        print('\n', post_count,'. There is no such record - ', job['title'], '-> Will be saved to the database.')



    urls.append(job['url_more'])
    data.append(job)

print('\n', len(data), 'records to be scraped... \n')

# create dataframe to add other information onto the same dataset
jobs_df = pd.DataFrame(data)
# create lists to populate with data from details page
updated_dates = []
full_descriptions = []
salaries = []
emp_types = []

# visit each post page to scrape further information ...
for url in urls:
    source2 = requests.get(url)
    soup2 = BeautifulSoup(source2.text, 'lxml')
    # # Pause the loop
    # sleep(randint(10, 17))
    #
    # # Monitor the requests
    request_count += 1
    elapsed_time = time() - start_time
    print('Request nr: {}; Frequency: {} requests per second; Status: {}'.format(request_count, request_count/elapsed_time, source2.status_code))

    updated = soup2.find('div', class_='c2').dl.findChildren('dd', recursive=False)[-1].text
    full_desc = soup2.find('div', class_='job-description').text
    # full_desc = full_desc.replace('\n', ' \n')
    full_desc = full_desc.replace("'", "")
    salary = soup2.find('div', class_='c1').dl.findChildren('dd', recursive=False)[-1].text
    emp_type = soup2.find('div', class_='c2').dl.findChildren('dd', recursive=False)[0].text

    updated_dates.append(updated)
    full_descriptions.append(full_desc)
    salaries.append(salary)
    emp_types.append(emp_type)

series = pd.Series(updated_dates)
jobs_df['updated'] = series.values
series = pd.Series(full_descriptions)
jobs_df['full_desc'] = series.values
series = pd.Series(salaries)
jobs_df['salary'] = series.values
series = pd.Series(emp_types)
jobs_df['emp_type'] = series.values



data = jobs_df.to_dict('records')



with open('data/jobs_posts.json', 'w', encoding='utf-8') as writeJSON:
    json.dump(data, writeJSON, ensure_ascii=False)
print('\nThe DF has been successfully translated into json, and there are: ', len(data), 'records.')
print(request_count, ' requests has been made.')

# Database
print('============================================== \n \n')
print('Database now... \n')






query_string = ""
posts_counter = 0
last_index = len(data)
# data['short_desc'] = data['short_desc'].encode('utf8')
for value in data:
    posts_counter += 1

    title = str(value['title'])
    company = str(value['company'])
    updated = str(value['updated'])
    location = str(value['location'])
    salary = str(value['salary'])
    short_desc = value['short_desc']
    full_desc = str(value['full_desc'])
    url = str(value['url_more'])
    emp_type = str(value['emp_type'])

# if item is not last on the list, put comma to separate each value in the query, else finish query
            # with semi-colon.
    if posts_counter != last_index:
        end_value = ","
    else:
        end_value = ";"

    query_string += "('" + title + "', '" + company + "', '" + updated + "', '" + location + "', '" + salary + "', '" + short_desc + "', '" + full_desc + "', '" + url + "', '" + emp_type + "')" + end_value

# error handling if no unique records found.
sql_query = "insert into jobs_posts (" \
                    "title, company, updated, location, salary, short_desc, full_desc, url, emp_type" \
                    ") values " + query_string
connection.execute(sql_query)
connection.close()



print('=================================')
print('JOBS.IE scraper is done.')
print('=================================')

# connection.execute(sql_query)
# connection.close()
