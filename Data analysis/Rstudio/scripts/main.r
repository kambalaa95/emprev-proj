# read json
library('jsonlite')
jobs_data <- fromJSON('data/input/it_jobs.json')

reed_data <- fromJSON('data/input/alljobids_sample1.json')
rm(reed_data)
View(reed_data)

###############################################################################################
#######       Top 10's
###############################################################################################

# job offers per company
table(jobs_data$company)
# Count of ads per company
x <- aggregate(data.frame(count = jobs_data$company), list(value = jobs_data$company), length)
View(x)
# sort by nr of advertisements
x <- x[with(x, order(-count)), ]
# top 10 advertising companies
top10_X <- x[1:10, ]
View(top10_X)

# Turns out most of companies are recruitment agencies.
table(jobs_data$comp_type)


# split into employers and agencies
employers <- subset(jobs_data, comp_type == "employer")
agencies <- subset(jobs_data, comp_type == "agency")


#count top 10 employers
emp_count <- aggregate(data.frame(count = employers$company), list(value = employers$company), length)
emp_ordered <- emp_count[with(emp_count, order(-count)), ]
top10_emp <- emp_ordered[1:10, ]
View(top10_emp)
barplot(top10_emp$count)

#count top 10 agencies
ag_count <- aggregate(data.frame(count = agencies$company), list(value = agencies$company), length)
ag_ordered <- ag_count[with(ag_count, order(-count)), ]
top10_ag <- ag_ordered[1:10, ]
View(top10_ag)
barplot(top10_ag$count)





###############################################################################################
#######       Distribution of job ads per location
###############################################################################################


length(unique(jobs_data$location)) # nr of unique locations

locations <- data.frame(loc_name = unique(jobs_data$location))
View(locations)
locations[1, ] # needs to be cleaned

# clean from line breaks
locations <- data.frame(
  lapply(locations, function(x){
    gsub('\n', '', x)
  })
)


#check if cleaned
View(locations)

# separate records with multiple locations
install.packages('data.table')
library('data.table')
multiple_locations <- data.frame(loc_name = locations[locations$loc_name %like% "/", ], row.names=NULL)
View(multiple_locations)
View(locations)
# now separate single locations

# combine multiple locations with locations
x <- rbind(locations, multiple_locations)
# filter out locations that are duplicated to get single ones
single_locations <- data.frame(loc_name = x[! duplicated(x, fromLast=TRUE) & seq(nrow(x)) <= nrow(locations), ])
View(single_locations)


# lets tidy multiple locations
class(multiple_locations$loc_name)
# data has to be of character format
multiple_locations$loc_name <- as.character(multiple_locations$loc_name)



# split locations separated by / into separate records
x <- strsplit(multiple_locations$loc_name, split = "/")
str(x)
# this will split each individual word into record
new_multiples <- data.frame(loc_name = unlist(x))
View(new_multiples)

# now change all dublin variations in new_multiples into just Dublin.
class(new_multiples$loc_name)
new_multiples$loc_name <- as.character(new_multiples$loc_name) # has to be character again

# change all the dublin variations
new_multiples[new_multiples$loc_name %like% "Dublin", ] <- "Dublin"
View(new_multiples)

# see how many unique locations there are
length(unique(new_multiples$loc_name))

# 85 is a lot considering there is 32 counties in ireland
# remove all white spaces that make the same locations seem different
new_multiples$loc_name <- gsub("[[:space:]]", "", new_multiples$loc_name)
length(unique(new_multiples$loc_name)) # 32 counties plus other custom locations e.g. Working from home

# Some of the same locations have city in it
new_multiples$loc_name <- gsub("city", "", new_multiples$loc_name)
length(unique(new_multiples$loc_name)) # 37 is much better

# create a list of unique locations from multiples
new_multiples <- data.frame(loc_name = unique(new_multiples$loc_name))
View(new_multiples)


############################
##### Now single locations
##########################
# create new dataset to be more neat
new_singles <- data.frame(loc_name = single_locations$loc_name)
# change dublin variations into dublin
new_singles[new_singles$loc_name %like% "Dublin", ] <- "Dublin"
# strip off city
new_singles$loc_name <- gsub("city", "", new_singles$loc_name)
# strip off whitespaces
new_singles$loc_name <- gsub("[[:space:]]", "", new_singles$loc_name)
# remove duplicates
new_singles <- data.frame(loc_name = unique(new_singles$loc_name))
str(new_singles)
View(new_singles)


# now mege the two datasets together and leave only unique values
unique_locations <- rbind(new_multiples, new_singles)
length(unique_locations$loc_name)

unique_locations <- data.frame(loc_name = unique(unique_locations$loc_name))
length(unique_locations$loc_name)
View(unique_locations)

write.csv(unique_locations, file = "unique_locations.csv")
write.csv(jobs_data, file = "jobs_data.csv", row.names = FALSE)

write.csv(multiple_locations, file ='multiple_locations.csv', row.names = FALSE)

exportJSON <- toJSON(multiple_locations)
write(exportJSON, "multiple_locations.json")


# now that you have all the unique locations identified you can start the python part.
# import it into text file and then read it with python
# further follow steps outlined in word# clean from line breaks


sum(new_jobs_data$location == 'galway')
# clustering and bag of words


# test if the number of Dublin occurences matches the cmd result from python code. 
new_jobs_data <- jobs_data[(jobs_data$location %like% "Dublin"), ]





install.packages('jsonlite')
library('jsonlite')
# locations from python
matched_locs <- fromJSON('data/matched_locs.json')
matched_locs
matched_locs <- data.frame(matched_locs)
class(matched_locs$matched_locs)
ads_distr <- data.frame(table(matched_locs))
colnames(ads_distr) <- c('loc_name', 'frequency')
ads_distr <- ads_distr[order(-ads_distr$Freq), ] # order descending
# write csv
write.csv(ads_distr, file = "ads_distr.csv", row.names = FALSE)
# or make json
exportJSON <- toJSON(ads_distr)
write(exportJSON, "ads_distr.json")



# original locations data
jobs_data <- fromJSON('data/it_jobs.json')
original_locs <- jobs_data$location
original_locs <- data.frame(original_locs)
View(original_locs)
# subset original locations and have a closer look at what happens
# with multiple locations
original_subset <- data.frame(original_locs[25:43, ])
colnames(original_subset) <- c('loc_name')
View(original_subset)
exportJSON <- toJSON(original_subset)
write(exportJSON, "test_subset.json")



ads_distr <- table(cat_locs$categorized_locations)
# ads_distr <- sort(ads_distr)
# ads_distr <- ads_distr[order()]
cols <- c('location', 'freq')
ads_distr <- data.frame(ads_distr)
# make csv for tableau
write.csv(ads_distr, file = "ads_distr.csv", row.names = FALSE)





class(ads_distr)
par(mai=c(0,2.5,0,0))
barplot(ads_distr)

barplot(ads_distr$matched_locs, ads_distr$Freq, xlab="Percentage", ylab="Proportion")


# works!! 
# Create vertical barplot in r
# make the chart in tableau
# use it in the website


class(categorized_locations)
categorized_locations <- as.data.frame(categorized_locations)
print(categorized_locations)
cat_locs <- as.data.frame(categorized_locations, row.names = NULL, optional = FALSE)
rm(cat_locs)




cat_locs <- sapply(categorized_locations, paste0, collapse=',')
cat_locs <- data.frame(loc_name=categorized_locations)
categorized_locations[ , ] <- as.data.frame(loc_name=categorized_locations)
rm(categorized_locations)
















# castlegate down 
data("mtcars")
mtcars[rownames(mtcars) %like% "Merc", ]
MyData[rownames(MyData) %like% "/", ]


library('lubridate')
# delete string from column
json_data$updated <- gsub("Updated", "", json_data$updated)
# convert to date
json_data$updated <- as.Date(json_data$updated, format = "%d/%m/%Y")
json_data[order(as.Date(json_data$updated, format="%d/%m/%Y")),]
