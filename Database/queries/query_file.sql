use jobs_db;
SET SQL_SAFE_UPDATES = 0; 

create table jobs_posts like irishjobs_posts;

# add primary key id to make it possible to query data better.
ALTER TABLE `jobs_posts` ADD `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY FIRST;
# see data
select * from jobs_posts;

# Check jobs_posts for duplicates and remove them
SELECT short_desc, COUNT(*) c FROM irishjobs_posts GROUP BY short_desc HAVING c > 1;

DELETE t1 FROM jobs_posts t1
        INNER JOIN
    jobs_posts t2 
WHERE
    t1.id < t2.id AND t1.short_desc = t2.short_desc;

SELECT COUNT(*) FROM jobs_posts; # there was 9 duplicates

# now create new table with the old records from irishjobs
CREATE TABLE irishjobs_posts like jobs_posts;
ALTER TABLE irishjobs_posts ADD COLUMN comp_type VARCHAR(100);

drop table jobs_posts;



# TEST TABLE DATA FROM CSV
create table test_table like jobs_posts;
alter table test_table modify column company varchar(255);
alter table test_table modify column location varchar(255);


select * from test_table;
SELECT short_desc FROM test_table;

# short_desc duplicates
SELECT title, COUNT(*) c FROM test_table GROUP BY title HAVING c > 1;
# title and short_desc duplicates
SELECT `title`,`short_desc`, COUNT(*) AS c FROM test_table 
GROUP BY `title`, `short_desc` HAVING c > 1;

# delete those duplicates
SELECT COUNT(*) FROM test_table;



# NOW MERGE test_table and jobs_posts table
SELECT COUNT(*) FROM test_table; # 1800
SELECT COUNT(*) FROM jobs_posts; # 156

INSERT IGNORE
  INTO test_table 
SELECT *
  FROM jobs_posts
     ;

SELECT COUNT(*) FROM test_table; # 1956 , still with duplicates




# irishjobs_posts 
SELECT COUNT(*) from irishjobs_posts;

# remove comp_type column
ALTER TABLE irishjobs_posts DROP COLUMN comp_type;

# insert records into test table
INSERT IGNORE INTO test_table SELECT * FROM irishjobs_posts;

SELECT COUNT(*) FROM test_table; # 4592 , still with duplicates

# short_desc duplicates
SELECT short_desc, COUNT(*) c FROM test_table GROUP BY short_desc HAVING c > 1;


ALTER TABLE `test_table` ADD `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY FIRST;
SELECT * FROM test_table;

# delete duplicates
DELETE t1 FROM test_table t1
        INNER JOIN
    test_table t2 
WHERE
    t1.id > t2.id AND t1.short_desc = t2.short_desc;

SELECT COUNT(*) FROM test_table; # 3817 no duplicates

ALTER TABLE test_table DROP COLUMN id;



# check data type
SELECT DATA_TYPE FROM jobs_posts.COLUMNS 
  WHERE table_name = 'tbl_name' AND COLUMN_NAME = 'col_name';



# short_desc duplicates
SELECT short_desc, COUNT(*) c FROM irishjobs_posts GROUP BY short_desc HAVING c > 1;
# title and short_desc duplicates
SELECT `title`,`short_desc`, COUNT(*) AS c FROM test_table 
GROUP BY `title`, `short_desc` HAVING c > 1;
# delete those duplicates
SELECT COUNT(*) FROM irishjobs_posts; #3052 before removing duplicates
ALTER TABLE `irishjobs_posts` ADD `id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY FIRST;

# delete duplicates
DELETE t1 FROM irishjobs_posts t1
        INNER JOIN
    irishjobs_posts t2 
WHERE
    t1.id > t2.id AND t1.short_desc = t2.short_desc;
    
SELECT COUNT(*) FROM test_table; # 2445 after removing duplicates
select * from test_table;
# 4320
ALTER TABLE irishjobs_posts DROP COLUMN id;



select count(*) from test_table; # 3886 unique records
INSERT IGNORE INTO test_table SELECT * FROM irishjobs_posts;
select count(*) from test_table; # 6005 unique records

