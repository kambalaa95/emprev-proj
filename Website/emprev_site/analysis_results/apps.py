from django.apps import AppConfig


class AnalysisResultsConfig(AppConfig):
    name = 'analysis_results'
