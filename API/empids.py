import requests, json
from requests.auth import HTTPBasicAuth
from time import sleep, time
import datetime
from random import randint
#open a file
fh = open('data/empids_53-100/empids_output90.json', 'w')
count1 = 0
count2 = 0



fh.write('[')
print('\nStart time: ', datetime.datetime.now().time(), '\n')
for i in range(655051, 1000000): # has been updated

        try:
            response = requests.get('https://www.reed.co.uk/api/1.0/search?keywords=IT&resultsToTake=100&employerId='+str(i),
                                    auth = HTTPBasicAuth('8839e748-3133-4163-a675-2bd058fae5f0', ''))

            print('EmployerId: ',i, ' out of 1000000', end="\r")

            json_resp = response.json() #json data object
        except:
            print('Something went wrong with response object')
            print('Request status code: ', response.status_code)

        sleep(randint(1,2)) # see if sleep will fix per hour requestself

        if i%100 == 0:
            count1 += 1
            print(str(count1)+'. 100 requests made  -    ', datetime.datetime.now().time())



        if i%500 == 0:
            count2 += 1
            print('Flush nr: '+str(count2), '  -   ', datetime.datetime.now().time(), '  -   ', i,' request')
            fh.flush()

        # if json object has one attribute, that means its a message.
        if len(json_resp) == 1:
            print(json_resp['message'])
            print('Request number: ', i)
            print('Time: ', datetime.datetime.now().time(), '\n')



        results = json_resp['results']
        # if no data in results, skip it.
        if len(results) == 0:
            continue

        print('Found record with employeeId - ', i, datetime.datetime.now().time())
        # Replace None values with string values to make a valid JSON
        for result in results: # for dict in the list of dicts
            for key, value in result.items(): # attributes w/ values
                if result[key] == None:
                    result[key] = "None"

        # each time records are found, write them to the reed_output file
        fh.write(str(json_resp)+",")





fh.write(']')
fh.close()



# need to go through 600 000 employer ids and save it in a file.





# https://www.reed.co.uk/developers/jobseeker
# http://docs.python-requests.org/en/master/user/authentication/
