import gensim
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords

# Let's create some documents.
raw_documents = ["I'm in love with IT job and being a developer.", "Doing Data Analytics is something that I love development.",
        "I have great potential with datum and web apps.", "My database software is not running well, better call SQL expert.",
        "Web development is easy money."]
print("Number of documents:",len(raw_documents))

# Tokenize the raw documents
tokenizer = RegexpTokenizer(r'\w+')
tok_docs = [[w.lower() for w in tokenizer.tokenize(text)]
            for text in raw_documents]
print(tok_docs)

# Remove stopwords
stop_words = set(stopwords.words('english'))
filtered_tok_docs = []
for tok_doc in tok_docs:
    filtered_tok_doc = [w for w in tok_doc if not w in stop_words]
    filtered_tok_docs.append(filtered_tok_doc)
print('Printing filtered_tok_docs: \n', filtered_tok_docs)

# Create dictionary
dictionary = gensim.corpora.Dictionary(filtered_tok_docs)
print("Number of words in dictionary:",len(dictionary))
# for i in range(len(dictionary)):
#     print(i, dictionary[i])

# Create corpus of bows
corpus = [dictionary.doc2bow(tok_doc) for tok_doc in tok_docs]
print(corpus)

# Create TF-IDF Model out of the docs corpus
tf_idf = gensim.models.TfidfModel(corpus)
print(tf_idf)


# Create sims object
sims = gensim.similarities.Similarity('data/',tf_idf[corpus],
                                      num_features=len(dictionary),
                                      num_best=len(corpus))
print(sims)
# print(type(sims))

# Input and process the query document all the way to passing it through tf-idf.
query_doc = [w.lower() for w in tokenizer.tokenize("Web development or developer required, not a software developer.")]
print(query_doc)
query_doc_bow = dictionary.doc2bow(query_doc)
print(query_doc_bow) # all of the words appearing in dictionary and query str
query_doc_tf_idf = tf_idf[query_doc_bow]
print(query_doc_tf_idf)
print('Dictionary indexes ', '-'*8, dictionary[0], dictionary[4],dictionary[18])



print(sims[query_doc_tf_idf]) # (doc_nr, sim_score)
