import csv, json
# open json and translate into python object
sourceFile = open('data/input/definetly_it_descriptions.json', 'rU', encoding='utf8')
json_data = json.load(sourceFile)

# csv
with open('data/output/definetly_it_descriptions.csv', 'w', newline='') as f:
    # writer object to write into csv
    thewriter = csv.writer(f)

    # columns
    thewriter.writerow(['title', 'company', 'updated', 'location', 'salary', 'short_desc', 'url_more', 'full_desc', 'emp_type'])
    # the records
    for row in json_data:
        title = row['title'].encode('utf-8')
        title = str(title).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")
        company = row['company'].encode('utf-8')
        company = str(company).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")
        updated = row['updated'].encode('utf-8')
        updated = str(updated).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")
        location = row['location'].encode('utf-8')
        location = str(location).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")
        salary = row['salary'].encode('utf-8')
        salary = str(salary).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")
        short_desc = row['short_desc'].encode('utf-8')
        short_desc = str(short_desc).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")
        url_more = row['url_more'].encode('utf-8')
        url_more = str(url_more).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")
        full_desc = row['full_desc'].encode('utf-8')
        full_desc = str(full_desc).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")
        emp_type = row['emp_type'].encode('utf-8')
        emp_type = str(emp_type).replace("b'", ' ').replace("'", ' ').replace("\\n", " ")


        thewriter.writerow([ title, company, updated, location, salary, short_desc, url_more, full_desc, emp_type])
