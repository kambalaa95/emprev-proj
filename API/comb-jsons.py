import json
import glob


# rfiles = glob.glob("data/empids_53-100/*.json")
rfiles = glob.glob("data/*.json")
output = []

for f in rfiles:
    with open(f, 'r', errors='ignore') as infile:
        output.append(json.loads(infile.read()))
        # print(infile)


# for item in output:
#     item = str(item)
#     fh.write(item+', ')
#
# fh.write(']')
#
# # fh.close()
# print(output)
# print(type(output))

output = json.dumps(output)
# print(output)

fh = open('data/alljobids.json', 'w', encoding='utf8')
fh.write(output)
fh.close()

print('File has been created.')
# print(output)
# with open("merged_file.json", "w") as outfile:
#      json.dump(result, outfile)
