###1. API (Python)
Gathers job posts data from reed.co.uk through the API provided. Subproject extracts first dataset by searching employerids (empids), then using jobids to extract the job posts (jobs-details.py).


###2. Scraping (Python/BeautifulSoup4)
*/irishjobs/*
*/jobs.ie/*
Two scrapers made in Python to gather data consistently from irishjobs.ie and jobs.ie.
The data is scraped and saved right into the database, as well as into the json file.

###3. Data analysis (Python/R)
**Ads distribution**

*Data Analysis/Rstudio/scripts/ads-distr.r*

*Data Analysis/Python/ads-distr/ads-distr.py*

The implemenetation starts in R file, where a list of unique locations is prepared.
Then in Python, record occurences of each unique location in the entire it_jobs dataset. 
Then back in R to create frequency distribution table and plot it. 

**Classifying jobs**

*Data Analysis/Python/classify-jobs*

Classifying job descriptions from all categories into 'Probably IT' and 'Definetly IT', using text similarities TF-IDF and Cosine similarity.

**Job areas - Topic modelling**

*Data Analysis/Python/Topic Modelling/LDA/notebooks*

Finding job areas through implementation of topic model LDA. The code in this subcomponent was written and ran using Jupyter Notebooks.

###4. Website (Python/Django/Bootstrap)

*Website/emprev_site/*

Final web application, written in Django. Set up to run locally with my local database. 

*Website/emprev.com/*

Just the html/js/css files of the webproject.

###5. Database (MySQL)

Contains script files where queries were written, also exported/imported data. 