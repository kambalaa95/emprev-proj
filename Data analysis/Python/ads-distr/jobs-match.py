import pandas as pd
import json
import csv
from pprint import pprint

# main dataset with
with open('data/input/all-it-jobs.json', encoding="utf8") as f:
    data = json.load(f)

# list with unique locations
u_locs = pd.read_csv("data/input/unique_locations.csv")
u_locs = u_locs['loc_name'].tolist()
# preprocess unique locations
u_locs = [u_loc.replace('\\xc2\\xa0', '') for u_loc in u_locs]
print(u_locs)


print(type(u_locs))
count = 0
list = []


jobs_df = pd.DataFrame(data=data, index=data[0:])

# create list, populate with location values from main dataset.
jobs_locs = []
for location in jobs_df['location']:
    location = location.replace('\n', '').replace('\\xc2\\xa0', '')
    location = location.lower()
    jobs_locs.append(location)

print('First jobs_locs len: \n', len(jobs_locs))
print('And type... ', type(jobs_locs))
print('\n ====================')




# go through unique locations
# go through jobs_locs
# if unique loc in jobs_locs[i]
# add current u_loc into the matched_locs[]
# this way all of the mentioned locations may be counted from the populated list.
matched_locs = []
for u_loc in u_locs:
    u_loc = u_loc.replace('\n', '').replace('\xc2\xa0', ' ')

    u_loc = u_loc.lower()
    for i in range(len(jobs_locs)):
        if u_loc in jobs_locs[i]:
            matched_locs.append(u_loc)

print("==========================")
print('The length of matched_locs is ...   ', len(matched_locs))
print("==========================")


json_dumps = json.dumps(matched_locs)

# JSON OUTPUT
with open('data/output/matched-locs.json', 'w') as f:
    f.write(json_dumps)
