import requests, json
from requests.auth import HTTPBasicAuth
from time import sleep, time
import pprint
from random import randint

# job details api
# https://www.reed.co.uk/api/{version number}/jobs/{Job Id} e.g. https://www.reed.co.uk/api/1.0/jobs/132


reed_output = open('data/allempids/allempids_53-91.json', 'r')
json_data = json.load(reed_output)

# print(json_data[0][0]['results'][0]['jobTitle'])

job_ids = []
count = 0

for empidlist in json_data:
    for dic in empidlist:
        for i in range(0, len(dic['results'])):
            count += 1
            job_id = dic['results'][i]['jobId']
            job_ids.append(job_id)
            print(count,'. ', job_id, ' - Appended to job_ids list.')


print('Length of the list: ', len(job_ids))
print(job_ids)

# now go loop through job ids and use each in the API call
full_jobs = []
jobid_count = 0
for id in job_ids:
    try:
        response = requests.get('https://www.reed.co.uk/api/1.0/jobs/'+str(id),
                                auth = HTTPBasicAuth('8839e748-3133-4163-a675-2bd058fae5f0', ''))
        full_jobs.append(response.json())

    except:
        print('Problem accessing jobid, onto the next one.')
    jobid_count += 1
    print('JobId: ',jobid_count, ' out of ', len(job_ids), end="\r")
    if jobid_count % 10 == 0:
        print('JobId: ',jobid_count, ' out of ', len(job_ids))
    sleep(randint(1, 2))
print('Length of full_jobs list: ', len(full_jobs))
# full_jobs = full_jobs.to_dict('records')

with open('data/alljobids3.json', 'w') as writeJSON:
    json.dump(full_jobs, writeJSON, ensure_ascii=False)
