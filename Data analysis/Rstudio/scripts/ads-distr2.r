###############################################################################################
#######       Distribution of job ads per location
###############################################################################################


# it jobs file
library('jsonlite')
jobs_data <- fromJSON('data/input/all-it-jobs.json')
# 1.Identify how many unique locations there are are in the data set.
# End goal is to have every unique location mentioned in the dataset
# to be embedded into the list. Further this list will be used
# to count the occurences of each unique location in the dataset. 
###################################################################

# a lot because of many variations of the same location.
# Dublin, Dublin / Cork

jobs_data <- fromJSON('data/input/all-it-jobs.json')

length(unique(jobs_data$location)) # 810 unique locations
locations <- data.frame(loc_name = unique(jobs_data$location))

View(locations)

locations[1, ] # needs to be cleaned from  '\n'

# clean from line breaks
locations <- data.frame(
  lapply(locations, function(x){
    gsub('\n', '', x)
  })
)

#check if cleaned
View(locations)

# separate records with multiple locations
# install.packages('data.table')
# library('data.table')

multiple_locations <- data.frame(loc_name = locations[locations$loc_name %like% "/", ], row.names=NULL)
View(multiple_locations)

View(locations)

# write.csv(multiple_locations, file ='multiple_locations.csv', row.names = FALSE)

# now separate single locations
# combine multiple locations with locations
x <- rbind(locations, multiple_locations)
# filter out locations that are duplicated to get single ones
single_locations <- data.frame(loc_name = x[! duplicated(x, fromLast=TRUE) & seq(nrow(x)) <= nrow(locations), ])

View(single_locations)


# lets tidy multiple locations
class(multiple_locations$loc_name)

# split locations separated by / into separate records
multiple_locations$loc_name <- as.character(multiple_locations$loc_name)
x <- strsplit(multiple_locations$loc_name, split = "/")

# this will split each individual word into record
new_multiples <- data.frame(loc_name = unlist(x))
View(new_multiples)

# now change all dublin variations in new_multiples into just Dublin.
class(new_multiples$loc_name) # currently factor

new_multiples$loc_name <- as.character(new_multiples$loc_name) # has to be character again
# dublin has other different variations like Dublin North. 
new_multiples[new_multiples$loc_name %like% "Dublin", ] <- "Dublin"
View(new_multiples)
length(unique(new_multiples$loc_name)) # 196

# Some of the same locations have city in it
new_multiples$loc_name <- gsub("city", " ", new_multiples$loc_name)
length(unique(new_multiples$loc_name)) # 193 
# 196 is still way too much, considering there is 32 counties in ireland

# it looks like because of the \xc\xa0 characters, unique() is skipping a lot of duplicates.

# remove all white spaces that make the same locations seem different
# not necessary anymore
# new_multiples$loc_name <- gsub("[[:space:]]", "", new_multiples$loc_name)


# still alot and we see why. The \xc2\xa0 characters. 
View(new_multiples)

# if we remove it this way, it will substitute the real character with space.
new_multiples[19, "loc_name"]
gsub("[$,\\xc2\\xa0]", " ", as.character(new_multiples[19, "loc_name"]))

# python ...
write.csv(new_multiples$loc_name, file = "data/output/filter-nonutf.csv")

# ... aaaand repoenning processed list. 
clean_multiples <- fromJSON('data/input/filtered-nonutf.json')
length(unique(clean_multiples)) # same as new_multiples

new_multiples[, 1] <- clean_multiples
# 32 counties + locations like 'mainland europe' or 'working from home'.

# create a list of unique locations from multiples
new_multiples <- data.frame(loc_name = unique(new_multiples$loc_name))
View(new_multiples)

# Now clean single locations

# create new dataset to be more neat
new_singles <- data.frame(loc_name = single_locations$loc_name)
# change dublin variations into dublin
new_singles[new_singles$loc_name %like% "Dublin", ] <- "Dublin"
# strip off city
new_singles$loc_name <- gsub("city", "", new_singles$loc_name)
# python ...
write.csv(new_singles$loc_name, file = "data/output/filter-nonutf.csv")


# ... aaand we're back again.
clean_singles <- fromJSON('data/input/filtered-nonutf.json')
length(unique(clean_singles)) # same as new_multiples
# strip off whitespaces
new_singles$loc_name <- gsub("[[:space:]]", "", new_singles$loc_name)
# remove duplicates
new_singles <- data.frame(loc_name = unique(new_singles$loc_name))
str(new_singles)
View(new_singles)

# now mege the two datasets together and leave only unique values
unique_locations <- rbind(new_multiples, new_singles)
length(unique(unique_locations$loc_name))
# trim duplicates again just in case
unique_locations <- data.frame(loc_name = unique(unique_locations$loc_name))
length(unique_locations$loc_name) # 116
View(unique_locations)

write.csv(unique_locations, file = "data/output/unique_locations.csv")

# now that you have all the unique locations identified you can start the python part.
# reading it with python and follwoing further steps outlined
# in the word doc.


##############################################################
# LOCATIONS ARE NOW PREPARED IN PYTHON FOR THE COUNT
############################################################
# install.packages('jsonlite')
library('jsonlite')
# locations from python
matched_locs <- fromJSON('data/input/matched-locs.json')
matched_locs <- data.frame(matched_locs)

ads_distr <- data.frame(table(matched_locs))
colnames(ads_distr) <- c('loc_name', 'frequency')

# order descending to see the top frequent locations
ads_distr <- ads_distr[order(-ads_distr$frequency), ] 
# write csv
write.csv(ads_distr, file = "data/output/ads_distr.csv", row.names = FALSE)
# Worked!! 

plot(ads_distr$loc_name, ads_distr$frequency)


# or make json
exportJSON <- toJSON(ads_distr)
write(exportJSON, "data/output/ads_distr.json")
# Now make the chart in tableau and use it in the website



# original locations data to compare with matched locations.
jobs_data <- fromJSON('data/input/it_jobs.json')
original_locs <- jobs_data$location
original_locs <- data.frame(original_locs)
View(original_locs)
# subset original locations and have a closer look at what happens
# with multiple locations when processed in jobs-match.py
original_subset <- data.frame(original_locs[25:43, ])
colnames(original_subset) <- c('loc_name')
View(original_subset)
exportJSON <- toJSON(original_subset)
write(exportJSON, "data/output/test_subset.json")







# only top 20
locs_top20 <- ads_distr[0:20,]

library(ggplot2)
#install.packages('gridExtra')
library(gridExtra)

#Build Function to Return Element Text Object
rotatedAxisElementText = function(angle,position='x'){
  angle     = angle[1]; 
  position  = position[1]
  positions = list(x=0,y=90,top=180,right=270)
  if(!position %in% names(positions))
    stop(sprintf("'position' must be one of [%s]",paste(names(positions),collapse=", ")),call.=FALSE)
  if(!is.numeric(angle))
    stop("'angle' must be numeric",call.=FALSE)
  rads  = (angle - positions[[ position ]])*pi/180
  hjust = 0.5*(1 - sin(rads))
  vjust = 0.5*(1 + cos(rads))
  element_text(angle=angle,vjust=vjust,hjust=hjust)
}


a <- 90

p <- ggplot(data=locs_top20, aes(x=reorder(locs_top20$loc_name, -locs_top20$frequency), y=locs_top20$frequency)) +
  geom_bar(stat="identity", color="#57b4e9", fill="#57b4e9")+ 
  theme(axis.text.x = rotatedAxisElementText(a,'x')) +
  labs(title = sprintf("Rotated %s",a))
p
p <- ggplotly(p)

api_create(p, filename = "ads-distr-test")













install.packages("devtools")  # so we can install from github
library("devtools")
install_github("ropensci/plotly")  # plotly is part of ropensci
library(plotly)

# example
p <- plot_ly(midwest, x = ~percollege, color = ~state, type = "box")
p


# Save your authentication credentials
Sys.setenv("plotly_username"="kambala")
Sys.setenv("plotly_api_key"="3hwk8uS8dkSkbkAkFEqr")

library(plotly)
p <- plot_ly(midwest, x = ~percollege, color = ~state, type = "box")
api_create(p, filename = "r-docs-midwest-boxplots")

