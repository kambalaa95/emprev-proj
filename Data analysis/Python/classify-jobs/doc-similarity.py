import gensim
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from gensim.utils import simple_preprocess # CHANGED DICTIONARY
import json
import pandas as pd
import spacy
import re

# Load tokenized it descriptions
tok_docs = open('data/it_tok_docs.json')
tok_docs = json.load(tok_docs)

# Load dictionary
dictionary = gensim.corpora.Dictionary.load('tmp/it_dictionary.dict')

# Create corpus of BOWs
corpus = [dictionary.doc2bow(tok_doc) for tok_doc in tok_docs]
# print("CORPUS of BOWs- ", len(corpus),":\n", corpus)
print(type(corpus))

# Create TF-IDF Model
tf_idf = gensim.models.TfidfModel(corpus)

# Create Similarities object
sims = gensim.similarities.Similarity('data/',tf_idf[corpus],
                                      num_features=len(dictionary),
                                      num_best=10)

# #######################################################################################################################
#
# # Load query docs - records from All jobs
query_data = open('data/alljobids.json', encoding="utf8")
query_data = json.load(query_data)
query_data = pd.DataFrame(query_data)
query_descriptions = query_data['jobDescription']
# remove html tags from query_descriptions
def clean_html(query_description):
  clean_re = re.compile('<.*?>')
  clean_text = re.sub(clean_re, '', query_description)
  return clean_text
# apply on each description
query_descriptions = [clean_html(str(query_description)) for query_description in query_descriptions]
# print(query_descriptions)
#
# # Tokenize query.
# tokenizer = RegexpTokenizer(r'\w+')
# query_tok_docs = [[ w for w in tokenizer.tokenize(text)]
#              for text in query_descriptions]
# print('Tokenized query_docs:\n',query_tok_docs, '\nSample len: ',len(query_tok_docs[0]))
#
# # Remove stopwords
# stop_words = set(stopwords.words('english'))
# query_tok_docs = [[w for w in query_tok_doc if not w in stop_words] for query_tok_doc in query_tok_docs]
# print('Without stop_words:\n',query_tok_docs, '\nSample len: ',len(query_tok_docs[0]))
#
# # Lemmatize query docs
# nlp = spacy.load('en', disable=['parser', 'ner'])
# tags = ['NOUN', 'ADJ']
# def lemmatize(descs, tags=['NOUN', 'ADJ']): # filter noun and adjective
#     output = []
#     for sent in descs:
#         doc = nlp(" ".join(sent))
#         output.append([token.lemma_ for token in doc if token.pos_ in tags])
#     return output
# query_tok_docs = lemmatize(query_tok_docs)
# print("Lemmatized query docs:\n", query_tok_docs, '\nSample len: ', len(query_tok_docs[0]))
# # Save the tokenized query corpus
# with open('data/query_tok_docs.json', 'w') as outfile:
#     json.dump(query_tok_docs, outfile)
#####################################################################################################

# Load tokenized query descriptions
query_tok_docs = open('data/query_tok_docs.json')
query_tok_docs = json.load(query_tok_docs)
print('QUERY TOK DOCS LEN:\n', len(query_tok_docs))
# # create corpus of bows, tf_idf.
query_bow = [dictionary.doc2bow(query_tok_doc) for query_tok_doc in query_tok_docs]

query_doc_tf_idf = tf_idf[query_bow]
print("Query tf_idf:\n", query_doc_tf_idf)
# Similarities between query and main corpus documents.
sims = sims[query_doc_tf_idf]
print("Similarities lists:\n", sims, '\nLen of sims array: ', len(sims),'\n', '-'*300)
#
# Top n matching docs
definetly_it = []
probably_it = []
# for each similarity in similarities
#   se

for i in range(len(sims)):
    def_sim_count = 0
    prob_sim_count = 0
    for doc_index, sim_score in sims[i]:  # for each array similarity array
        sim_document = tok_docs[doc_index]
        # condition if doc similarity score > 0.65 for at least 2 records
        if sim_score >= 0.25 and sim_score <= 0.44:
            prob_sim_count += 1
            print(prob_sim_count, '. ', sim_score, ' -> [',doc_index,']', sim_document)
            if prob_sim_count >= 2:
                print('This is probably an it record:\n', query_descriptions[i], '\n', '-' * 300)
                probably_it.append(query_descriptions[i])
                break
        elif sim_score >= 0.45:
            def_sim_count += 1
            # if we have at least two of such scores
            print(prob_sim_count, '. ', sim_score, ' -> [',doc_index,']', sim_document)
            if def_sim_count >= 2:
                print('This doc is most likely IT related:\n',
                      query_descriptions[i], '\n')
                definetly_it.append(query_descriptions[i])
                break

print('='*500)
print(len(sims))


print('Classified probably IT descriptions:\n', probably_it, '\nLength of the set: ', len(probably_it))
with open('data/probably_it_descriptions.json', 'w') as outfile:
    json.dump(probably_it, outfile)

print('Classified definetly IT descriptions:\n', definetly_it, '\nLength of the set: ', len(definetly_it))
with open('data/definetly_it_descriptions.json', 'w') as outfile:
    json.dump(definetly_it, outfile)
# print('Dictionary index: ', dictionary[94])

# if similarity]
# print(query_data['jobDescription'])
