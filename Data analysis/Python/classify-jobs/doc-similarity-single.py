import gensim
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from gensim.utils import simple_preprocess # CHANGED DICTIONARY
import json
import pandas as pd
import spacy
import re

# Load tokenized it descriptions
tok_docs = open('data/it_tok_docs.json')
tok_docs = json.load(tok_docs)

# Load dictionary
dictionary = gensim.corpora.Dictionary.load('tmp/it_dictionary.dict')

# Create corpus of BOWs
corpus = [dictionary.doc2bow(tok_doc) for tok_doc in tok_docs]
print("CORPUS of BOWs- ", len(corpus),":\n", corpus)
print(type(corpus))

# Create TF-IDF Model
tf_idf = gensim.models.TfidfModel(corpus)

# Create Similarities object
sims = gensim.similarities.Similarity('data/',tf_idf[corpus],
                                      num_features=len(dictionary),
                                      num_best=10)
#######################################################################################################################
# Load query docs - records from All jobs
query_descriptions = open('data/sample_desc.txt', 'r', encoding='utf8')
query_descriptions = query_descriptions.readlines()
query_descriptions = " ".join(query_descriptions)
print(query_descriptions)

# # Tokenize query.
tokenizer = RegexpTokenizer(r'\w+')
query_tok_docs = [w for w in tokenizer.tokenize(query_descriptions)]
print('Tokenized query_docs:\n',query_tok_docs, '\nSample len: ',len(query_tok_docs))

# Remove stop words
stop_words = set(stopwords.words('english'))
query_tok_docs = [w.lower() for w in query_tok_docs if not w in stop_words]
print('Without stop_words:\n',query_tok_docs, '\nSample len: ',len(query_tok_docs))

# Lemmatize the query doc
nlp = spacy.load('en', disable=['parser', 'ner'])
tags = ['NOUN', 'ADJ']
doc = nlp(" ".join(query_tok_docs[i] for i in range(len(query_tok_docs))))
query_tok_docs = [token.lemma_ for token in doc if token.pos_ in tags]
print("Lemmatized query docs:\n", query_tok_docs, '\nSample len: ', len(query_tok_docs))
# create corpus of bows, tf_idf.
query_bow = dictionary.doc2bow(query_tok_docs)
print(query_bow)
query_doc_tf_idf = tf_idf[query_bow]
print("Query tf_idf:\n", query_doc_tf_idf)
# Similarities between query and main corpus documents.
sims = sims[query_doc_tf_idf]
print("Similarities:\n", sims, '\nLen of sims array: ', len(sims))

it_descriptions = []
# Categorizaiton of data based on similarity of queried text with other IT descriptions in the corpus.
sim_docs_count = 0
sim_count = 0
for doc_index, sim_score in sims:
    sim_count += 1
    sim_document = tok_docs[doc_index]
    # print(sim_count, '. ', sim_score, ' -> ',sim_document)
    if sim_score > 0.3:
        sim_docs_count += 1
        print(sim_count, '. ', sim_score, ' -> [',doc_index,']', sim_document)

    if sim_docs_count >= 2:
        print('More than 2 records have sim score > 0.60. This sample job description is probably IT related:\n', query_tok_docs,'\n')
        it_descriptions.append(query_descriptions)
        break
    # if sim score  between 40-59 maybe yes
    # if below probably not
if sim_docs_count < 2:
    print('Not enough similarities found. This sample job description is not IT related\n')

print('Classified IT descriptions:\n', it_descriptions, '\nLen of it_descriptions: ', len(it_descriptions))
with open('data/it_descriptions.json', 'w') as outfile:
    json.dump(it_descriptions, outfile, indent=3)
print('Jobs have been classified and written to it_descriptions.json')
